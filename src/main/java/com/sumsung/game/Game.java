/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumsung.game;

import java.util.Scanner;

/**
 *
 * @author sumsung
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Plase input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            } else {
                System.out.println("Error: table at row and col "
                        + "is not empty!!!");
            }
        }
    }

    public void showTurn() {
        System.out.println(table.getCerrentPlayer().getName() + " turn");
    }

    public void run() {
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            input();
            table.checkWin();
            if (checkfinish()) {
                endGame();
                break;
            }
            table.switchPlayer();
            table.roundUp();
            endGame();
        }
    }

    private void endGame() {
        table.showScore(playerX);
        table.showScore(playerO);
        showBye();
    }

    private boolean checkfinish() {
        if (table.isFinish()) {
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!");
            }
            showTable();
            if (playAgain()) {
                newGame();
            } else {
                return true;
            }
        }
        return false;
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public boolean playAgain() {
        System.out.println("Do you want play game XO again Yes/No???");
        String answer = kb.next();
        if (answer.equals("Yes") || answer.equals("yes")) {
            return true;
        } else if (answer.equals("No") || answer.equals("no")) {
            return false;
        } else {
            System.out.println("Plase input again!!!");
            return playAgain();
        }
    }

    public void showBye() {
        System.out.println("Bye Bye...!!!");
    }
}
