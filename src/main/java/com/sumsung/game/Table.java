/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumsung.game;

/**
 *
 * @author sumsung
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish;
    private int lastcol;
    private int lastrow;
    private int round = 1;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getCerrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerO) {
            currentPlayer = playerX;
        } else {
            currentPlayer = playerO;
        }

    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkX_Left() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (row == col && table[row][col] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
    }

    void checkX_Right() {
        int col = 2;
        int row = 0;
        for (int round = 0; round < 3; round++) {
            if (table[row][col] != currentPlayer.getName()) {
                return;
            }
            col--;
            row++;
        }
        finish = true;
        winner = currentPlayer;
    }

    void checkDraw() {
        if (round == 9 && finish == false && winner == null) {
            finish = true;
        }
        playerO.draw();
        playerX.draw();
    }

    public void checkWin() {
        checkCol();
        checkRow();
        checkX_Left();
        checkX_Right();
        checkDraw();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

    public void roundUp() {
        round++;
    }

    public void showScore(Player player) {
        System.out.println(player.getName()
                + " --> score win is " + player.getWin()
                + "\n" + "  --> score lose is " + player.getLose()
                + "\n" + "  --> score draw is " + player.getDraw());
    }

}
